#!/bin/bash

##################################################
# THIS IS ONLY USED FOR FORCING BITBUCKET UPDATE #
##################################################

HOMEPATH="/home/pi"
UPDATEPATH="$HOMEPATH/origin"
REMOTE_URL="https://update.MISSINGURL/player/update.zip"

if [ -e /home/pi/script.lock ]; then
    echo "script is already running"
    exit 1
else
    touch /home/pi/script.lock
    echo "Running script"
fi

execute_command() {
    echo "Executing: $1"
    eval "$1"
}
cd /home/pi/

url=$(grep 'url:' /home/pi/player/docs/synchro.md | cut -d'"' -f2)
domain=$(echo "$url" | awk -F[/:] '{print $4}' | awk -F. '{print $(NF-1)"."$NF}')
REMOTE_URL=$(echo "$REMOTE_URL" | sed "s|MISSINGURL|$domain|g")
sed -i "s@MISSINGURL@$domain@g" /home/pi/player/update/update.sh

echo \\n--- START BITBUCKET UPDATE SCRIPT ---

echo "creating temporary update directory"
if [ ! -d "$UPDATEPATH" ]; then
    mkdir -p $UPDATEPATH
fi


wget --progress=dot:mega "$REMOTE_URL" -P "$UPDATEPATH"
EXPECTED_CHECKSUM=$(curl -sI "$REMOTE_URL" | grep -i '^ETag:' | tr -d '\r' | awk '{print $2}' | tr -d '"')
ACTUAL_CHECKSUM=$(md5sum "$UPDATEPATH/update.zip" | awk '{print $1}')

if [ "$EXPECTED_CHECKSUM" != "$ACTUAL_CHECKSUM" ]; then
    echo "Checksum mismatch. Aborting update."
    rm -rf "$UPDATEPATH"
    rm /home/pi/script.lock
    exit 1
fi


sudo sed -i 's@/home/pi/player/update.sh@/home/pi/confighub/update/update.sh@g' /etc/rc.local

execute_command "sudo mv /home/pi/player/update/update.sh /home/pi/"
execute_command "sudo mkdir /home/pi/tmp"
execute_command "sudo mkdir /home/pi/tmp/status"
execute_command "sudo mkdir /home/pi/tmp/files"
execute_command "sudo cp /home/pi/player/config.js /home/pi/tmp/config.js"
execute_command "sudo cp /home/pi/player/status/playlist.json /home/pi/tmp/status/playlist.json"
execute_command "sudo cp /home/pi/player/status/snapshot.png /home/pi/tmp/status/snapshot.png"
execute_command "sudo cp /home/pi/player/status/status.html /home/pi/tmp/status/status.html"
execute_command "sudo cp /home/pi/player/files/*.mp4 /home/pi/tmp/files/"
execute_command "sudo cp /home/pi/player/files/*.bkup /home/pi/tmp/files/"
execute_command "sudo cp /home/pi/simplemenu/conf.txt /home/pi/tmp/conf.txt"
execute_command "mkdir /home/pi/backup"
execute_command "sudo mv /home/pi/posterConfig /home/pi/backup"
execute_command "sudo mv /home/pi/simplemenu /home/pi/backup"
execute_command "sudo mv /home/pi/updates /home/pi/backup"
execute_command "sudo mv /home/pi/player /home/pi/backup"

execute_command "chmod +x /home/pi/update.sh"
execute_command "sudo /home/pi/update.sh"

execute_command "sudo mv /home/pi/tmp/config.js /home/pi/player/config.js"
execute_command "sudo mv /home/pi/tmp/conf.txt /home/pi/dashboard/conf.txt"
execute_command "sudo mv /home/pi/tmp/status/playlist.json /home/pi/player/status/playlist.json"
execute_command "sudo mv /home/pi/tmp/status/snapshot.png /home/pi/player/status/snapshot.png"
execute_command "sudo mv /home/pi/tmp/status/status.html /home/pi/player/status/status.html"
execute_command "sudo mv /home/pi/tmp/files/*.mp4 /home/pi/player/files/"
execute_command "sudo mv /home/pi/tmp/files/*.bkup /home/pi/player/files/"
execute_command "sudo rm -rf /home/pi/tmp"

execute_command "sudo cp /home/pi/confighub/version.md /home/pi/version.md"
execute_command "sudo rm /home/pi/version.txt"
execute_command "sudo rm /home/pi/update.sh"
execute_command "sudo mv /home/pi/update.log /home/pi/confighub/update/"

execute_command "sudo chown -R pi:pi /home/pi"
execute_command "sudo chown -R pi:pi /home/pi/player/backup"
execute_command "sudo chown pi:pi /home/pi/version.md"
execute_command "sudo rm /home/pi/script.lock"
find /home/pi/ -maxdepth 1 -type f -name '*update*' -exec rm -f {} +

execute_command "sudo reboot"

sysdate=$(date +%Y-%m-%d%t%H:%M:%S)
echo \\n--- $sysdate ---
echo --- END OF UPDATE SCRIPT ---\\n
