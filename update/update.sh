#!/bin/bash

#########################################################################################
# DO NOT MODIFY THE UPDATE SCRIPT IF IT HAS NOTHING TO DO WITH THE UPDATE OR IMAGE ITSELF
#########################################################################################

REMOTE_URL="https://update.MISSINGURL/player/update.zip"
VERSION_URL="https://update.MISSINGURL/player/version.md"
HOMEPATH="/home/pi"
UPDATEPATH="$HOMEPATH/origin"
SCRIPTPATH="$HOMEPATH/confighub/setup"
REMOTE_VERSION=$(curl -s "$VERSION_URL")

version_gt() {
    remote_version=$(echo "$1" | cut -d'-' -f1 | tr -d '.')
    local_version=$(echo "$2" | cut -d'-' -f1 | tr -d '.')
    [ "$remote_version" -gt "$local_version" ]
}

if [ ! -f "/home/pi/debug" ]; then
    ### pipe all output to stdout and update.log ###
    script_dir="$(dirname -- "$(readlink -f -- "$0")")"
    output_pipe=/tmp/$$.tmp
    trap "rm -f $output_pipe" EXIT
    mknod $output_pipe p
    tee <$output_pipe -a $script_dir/update.log &
    exec 1>$output_pipe 2>&1

    ### start with some infos for the log file ###
    sysdate=$(date +%Y-%m-%d%t%H:%M:%S)
    echo \\n--- START UPDATE SCRIPT ---
    echo --- $sysdate ---\\n

    ### update directory check ###
    echo "creating temporary update directory"
    if [ ! -d "$UPDATEPATH" ]; then
        mkdir -p $UPDATEPATH
    fi

    if [ -f "$HOMEPATH/player/version.md" ]; then
        LOCAL_VERSION=$(cat "$HOMEPATH/player/version.md")
        echo "Local player version is $LOCAL_VERSION"
    else
        echo "Local Version not found. Reset to 0.0"
        LOCAL_VERSION="0.0"
    fi

    if version_gt "$REMOTE_VERSION" "$LOCAL_VERSION"; then
        echo "Updating from $LOCAL_VERSION to $REMOTE_VERSION with already downloaded file"

        unzip "$UPDATEPATH/update.zip" -d "$UPDATEPATH" >/dev/null
        rsync -avz --exclude "config.js" --exclude "update.zip" --exclude "conf.txt" "$UPDATEPATH/" "$HOMEPATH/" >/dev/null
        echo "Update from $LOCAL_VERSION to $REMOTE_VERSION completed."
        sudo rm -rf $UPDATEPATH
        sudo chown -R pi:pi /home/pi

        if [ -d "/home/pi/backup" ]; then
            echo "Backup directory exists. Removing its contents..."
            sudo rm -rf /home/pi/backup
            echo "Contents of the backup directory removed."
        fi

        ### script packages execution ###
        echo \\n EXECUTE ALL PACKAGE SCRIPTS in $SCRIPTPATH\\n
        if [ -d "$SCRIPTPATH" ]; then
            for script in $(find $SCRIPTPATH/ -name '*.sh'); do
                echo "executing $script"
                sh "$script"
            done
        else
            echo \\n !!! ERROR: script directory doesn\'t exist !!!\\n
        fi
    else
        echo "No new version available. Skipping update."
    fi

    ### end with some infos for the log file ###
    sysdate=$(date +%Y-%m-%d%t%H:%M:%S)
    echo \\n--- $sysdate ---
    echo --- END OF UPDATE SCRIPT ---\\n

fi
